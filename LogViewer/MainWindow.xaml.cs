﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace LogViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private RuleSetsContainer _ruleSetsContainer;

        private List<LogLine> _logHolder;
        private readonly LogViewModel _viewModel;

        public MainWindow()
        {
            InitializeComponent();
            _ruleSetsContainer = new RuleSetsContainer(new ObservableCollection<RuleSet>());
            _ruleSetsContainer.RuleSets.Add(new RuleSet("Нет подсветки","Набор правил подсветки по умолчанию"));
            _ruleSetsContainer.CurrentRuleSet = _ruleSetsContainer.RuleSets.FirstOrDefault();
            LabelRuleSetname.DataContext = _ruleSetsContainer;

            _logHolder = new List<LogLine>();
            _viewModel = new LogViewModel(_logHolder);
            ListBoxLog.DataContext = _viewModel;
        }

        private void MenuItemSelectRuleSet_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new RuleSetSettingsDialog((RuleSetsContainer)_ruleSetsContainer.Clone());
            if (dialog.ShowDialog() != true) return;

            _ruleSetsContainer = dialog.RuleSetsContainer;
            if (_ruleSetsContainer.CurrentRuleSet == null)
                _ruleSetsContainer.CurrentRuleSet = _ruleSetsContainer.RuleSets.FirstOrDefault();
            LabelRuleSetname.DataContext = _ruleSetsContainer;
            ListBoxLog.Items.Refresh();
            _ruleSetsContainer.UpdateRuleSets();
        }

        private void MenuItemRuleSetSettings_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new RuleSettingsDialog(_ruleSetsContainer.CurrentRuleSet);
            dialog.ShowDialog();
            LabelRuleSetname.DataContext = _ruleSetsContainer;
            ListBoxLog.Items.Refresh();
        }

        private void MenuItemOpenFile_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                _logHolder = new List<LogLine>();
                int index = 1;
                foreach (var line in File.ReadLines(dialog.FileName))
                    _logHolder.Add(new LogLine(index++, line));
                _viewModel.Lines = _logHolder;
            }
        }

        private void MenuItemSaveFile_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new SaveFileDialog();
            if (dialog.ShowDialog() == false) return;
            var builder = new StringBuilder();
            foreach (LogLine bString in _logHolder)
                builder.AppendLine(bString.Text);
            File.WriteAllText(dialog.FileName, builder.ToString());
        }

        private void ButtonPreviousMatch_OnClick(object sender, RoutedEventArgs e)
        {
            _viewModel.PreviousSearchMatch();
        }

        private void ButtonNextMatch_OnClick(object sender, RoutedEventArgs e)
        {
            _viewModel.NextSearchMatch();
        }

        private void ListBoxLog_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBoxLog.ScrollIntoView(ListBoxLog.SelectedItem);
        }
    }
}
