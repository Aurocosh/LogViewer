﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Media;

namespace LogViewer
{
    public class FormatDocBuilder
    {

        public static List<Run> FormatText(TextPointer firstIndex, TextPointer lastIndex, RuleSet ruleSet)
        {
            var textRange = new TextRange(firstIndex, lastIndex);
            var text = textRange.Text;
            return FormatText(text, ruleSet);
        }

        public static List<Run> FormatText(string text, RuleSet ruleSet)
        {
            var ruleMatches = new List<RegexRule>();
            foreach (Rule rule in ruleSet.Rules)
            {
                Regex regex = rule.Regex;
                if (regex == null) continue;

                MatchCollection matches = regex.Matches(text);
                foreach (Match match in matches)
                    ruleMatches.Add(new RegexRule(match.Index, match.Length, rule));
            }

            int lastEndIndex = -1;
            ruleMatches = ruleMatches.OrderBy(v => v.Start).ToList();
            var overlappingMatches = new List<RegexRule>();
            foreach (RegexRule ruleMatch in ruleMatches)
            {
                if (ruleMatch.Start > lastEndIndex)
                    lastEndIndex = ruleMatch.Length;
                else
                    overlappingMatches.Add(ruleMatch);
            }

            ruleMatches = ruleMatches.Except(overlappingMatches).ToList();

            var lastCharIndex = 0;
            var p = new List<Run>();
            foreach (RegexRule ruleMatch in ruleMatches)
            {
                if (lastCharIndex != ruleMatch.Start)
                {
                    var r = new Run();
                    r.Text = text.Substring(lastCharIndex, ruleMatch.Start - lastCharIndex);
                    p.Add(r);
                }
                var mr = new Run();
                mr.Text = text.Substring(ruleMatch.Start, ruleMatch.Length);
                mr.Foreground = new SolidColorBrush(ruleMatch.Rule.HighlightColor);
                p.Add(mr);
                lastCharIndex = ruleMatch.Start + ruleMatch.Length;
            }
            if (lastCharIndex != text.Length - 1)
            {
                var r = new Run();
                r.Text = text.Substring(lastCharIndex, text.Length - lastCharIndex);
                p.Add(r);
            }

            return p;
        }
    }
}
