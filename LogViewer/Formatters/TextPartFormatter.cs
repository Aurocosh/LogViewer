﻿using System.IO;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace LogViewer
{
    public static class TextPartFormatter
    {
        public static FlowDocument FormatText(FlowDocument document, RuleSet ruleSet)
        {
            var range = new TextRange(document.ContentStart, document.ContentEnd);
            return FormatText(range.Text, ruleSet);
        }

        public static FlowDocument FormatText(TextPointer firstPointer, TextPointer lastPointer, RuleSet ruleSet)
        {
            var range = new TextRange(firstPointer, lastPointer);
            return FormatText(range.Text, ruleSet);
        }

        public static FlowDocument FormatText(string text, RuleSet ruleSet)
        {
            var document = new FlowDocument();
            var paragraph = new Paragraph();
            document.Blocks.Add(paragraph);
            var runs = FormatDocBuilder.FormatText(text, ruleSet);
            foreach (Run run in runs)
                paragraph.Inlines.Add(run);
            return document;
        }
    }
}
