﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace LogViewer
{
    [ValueConversion(typeof(bool), typeof(SolidColorBrush))]
    public class SearchColorConverter : IValueConverter
    {
        private static readonly Dictionary<bool, SolidColorBrush> _searchColor = new Dictionary<bool, SolidColorBrush>()
        {
            {false, Brushes.AliceBlue},
            {true, Brushes.GreenYellow}
        };

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var isMatch = (bool) value;
            return _searchColor[isMatch];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
