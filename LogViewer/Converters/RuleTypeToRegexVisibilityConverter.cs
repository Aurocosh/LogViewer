﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace LogViewer
{
    [ValueConversion(typeof(RuleType), typeof(Visibility))]
    public class RuleTypeToRegexVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is RuleType))
                throw new ArgumentException("Value is not a RuleType");

            switch ((RuleType)value)
            {
                case RuleType.RegexRule:
                    return Visibility.Visible;
                case RuleType.KeyWordsRule:
                    return Visibility.Collapsed;
            }
            throw new ArgumentException("Value is not a valid");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
