﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace LogViewer
{
    [ValueConversion(typeof(RuleType), typeof(Visibility))]
    public class RuleTypeToKeywordVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is RuleType))
                throw new ArgumentException("Value is not a RuleType");

            switch ((RuleType)value)
            {
                case RuleType.RegexRule:
                    return Visibility.Collapsed;
                case RuleType.KeyWordsRule:
                    return Visibility.Visible;
            }
            throw new ArgumentException("Value is not a valid");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
