﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace LogViewer.Converters
{
    public class RuleTypeAndBoolToVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Any(v => v == null)) return Visibility.Collapsed;
            var ruleType = (RuleType)values[0];
            if (ruleType == RuleType.RegexRule) return Visibility.Collapsed;
            var isMouseOver = (bool)values[1];
            return isMouseOver ? Visibility.Visible : Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
