﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Documents;

namespace LogViewer
{
    class FlowDocumentMultiConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Any(v => v == null))
                return values[1];
            var markupRuleSet = (RuleSet) values[0];
            var text = (string) values[1];
            FlowDocument doc = TextPartFormatter.FormatText(text, markupRuleSet);
            return doc;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
