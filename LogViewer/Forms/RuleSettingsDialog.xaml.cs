﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LogViewer
{
    /// <summary>
    /// Interaction logic for RuleSettingsDialog.xaml
    /// </summary>
    public partial class RuleSettingsDialog : Window
    {
        private readonly RuleSet _ruleSet;

        public RuleSettingsDialog(RuleSet ruleSet)
        {
            InitializeComponent();
            _ruleSet = ruleSet;
            ListBoxRules.DataContext = ruleSet;
        }

        private void ButtonAccept_OnClick(object sender, RoutedEventArgs e)
        {
            _ruleSet?.UpdateRuleSet();
            DialogResult = true;
        }

        private void ButtonAddRule_OnClick(object sender, RoutedEventArgs e)
        {
            var rule = new Rule("New rule", RuleType.KeyWordsRule);
            rule.KeyWords.Add(new StringWrapper("keyword"));
            _ruleSet.Rules.Add(rule);
        }

        private void ButtonDeleteRule_OnClick(object sender, RoutedEventArgs e)
        {
            var rule = (Rule)ListBoxRules.SelectedItem;
            if (rule != null)
                _ruleSet.Rules.Remove(rule);
        }

        private void ButtonRemoveKeyWord_OnClick(object sender, RoutedEventArgs e)
        {
            var rule = (Rule)ListBoxRules.SelectedItem;
            rule?.KeyWords.Add(new StringWrapper("keyword"));
        }

        private void ButtonAddNewKeyWord_OnClick(object sender, RoutedEventArgs e)
        {
            var keyWord = (StringWrapper)ListBoxKeyWords.SelectedItem;
            if (keyWord == null) return;

            var rule = (Rule)ListBoxRules.SelectedItem;
            rule?.KeyWords.Remove(keyWord);
        }
    }
}
