﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Serialization;
using Microsoft.Win32;

namespace LogViewer
{
    /// <summary>
    /// Interaction logic for RuleSetSettingsDialog.xaml
    /// </summary>
    public partial class RuleSetSettingsDialog : Window
    {
        public RuleSetsContainer RuleSetsContainer { get; private set; }

        public RuleSetSettingsDialog(RuleSetsContainer ruleSetsContainer)
        {
            InitializeComponent();
            RuleSetsContainer = ruleSetsContainer;
            ListBoxRuleSets.DataContext = ruleSetsContainer;
        }

        private void ButtonAccept_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ButtonAddNewRuleSet_OnClick(object sender, RoutedEventArgs e)
        {
            var ruleSet = new RuleSet("Нет подсветки", "Набор правил подсветки по умолчанию");
            RuleSetsContainer.RuleSets.Add(ruleSet);
        }

        private void ButtonRemoveRuleSet_OnClick(object sender, RoutedEventArgs e)
        {
            RuleSet currentSet = RuleSetsContainer.CurrentRuleSet;
            RuleSetsContainer.RuleSets.Remove(currentSet);
            RuleSetsContainer.CurrentRuleSet = null;
        }

        private void MenuItemSaveRule_OnClick(object sender, RoutedEventArgs e)
        {
            if (RuleSetsContainer.CurrentRuleSet == null)
            {
                MessageBox.Show(this, "Ничего не выбрано");
                return;
            }

            var dialog = new SaveFileDialog { Filter = "XML files (*.xml)|*.xml" };
            if (dialog.ShowDialog() != true) return;

            var serializer = new RuleSetSerializer();
            var saved = serializer.Serialize(RuleSetsContainer.CurrentRuleSet, dialog.FileName);
            if (!saved)
                MessageBox.Show(this, "Не удалось сохранить правило: " + serializer.LastError);
        }

        private void MenuItemLoadRule_OnClick(object sender, RoutedEventArgs e)
        {
            if (RuleSetsContainer.CurrentRuleSet == null)
            {
                MessageBox.Show(this, "Ничего не выбрано");
                return;
            }

            var dialog = new OpenFileDialog { Filter = "XML files (*.xml)|*.xml" };
            if (dialog.ShowDialog() != true) return;

            var serializer = new RuleSetSerializer();
            RuleSet ruleSet = serializer.Deserialize(dialog.FileName);
            if (ruleSet != null)
                RuleSetsContainer.RuleSets.Add(ruleSet);
            else
                MessageBox.Show(this, "Не удалось загрузить правило: " + serializer.LastError);
        }

        private void MenuItemSaveRuleContainer_OnClick(object sender, RoutedEventArgs e)
        {
            if (RuleSetsContainer.CurrentRuleSet == null)
            {
                MessageBox.Show(this, "Ничего не выбрано");
                return;
            }

            var dialog = new SaveFileDialog { Filter = "XML files (*.xml)|*.xml" };
            if (dialog.ShowDialog() != true) return;

            var serializer = new RuleContainerSerializer();
            var saved = serializer.Serialize(RuleSetsContainer, dialog.FileName);
            if (!saved)
                MessageBox.Show(this, "Не удалось сохранить набор правил: " + serializer.LastError);
        }

        private void MenuItemLoadRuleContainer_OnClick(object sender, RoutedEventArgs e)
        {
            if (RuleSetsContainer.CurrentRuleSet == null)
            {
                MessageBox.Show(this, "Ничего не выбрано");
                return;
            }

            var dialog = new OpenFileDialog { Filter = "XML files (*.xml)|*.xml" };
            if (dialog.ShowDialog() != true) return;

            var serializer = new RuleContainerSerializer();
            RuleSetsContainer container = serializer.Deserialize(dialog.FileName);
            if (container != null)
            {
                RuleSetsContainer = container;
                container.CurrentRuleSet = container.RuleSets.FirstOrDefault();
                ListBoxRuleSets.DataContext = RuleSetsContainer;
            }
            else
                MessageBox.Show(this, "Не удалось загрузить правило: " + serializer.LastError);
        }
    }
}
