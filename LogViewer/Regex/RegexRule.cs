﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogViewer
{
    public class RegexRule
    {
        public int Start { get; }
        public int Length { get; }
        public Rule Rule { get; }

        public RegexRule(int start, int length, Rule rule)
        {
            Start = start;
            Length = length;
            Rule = rule;
        }
    }
}
