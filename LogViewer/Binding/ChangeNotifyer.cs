﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using LogViewer.Annotations;

namespace LogViewer
{
    public abstract class ChangeNotifyer:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
