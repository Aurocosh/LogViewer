﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogViewer
{
    public enum RuleType
    {
        [Description("Регулярное выражение")]
        RegexRule,
        [Description("Ключевые слова")]
        KeyWordsRule
    }
}
