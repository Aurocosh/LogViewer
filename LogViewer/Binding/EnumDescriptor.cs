﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace LogViewer
{
    public class EnumDescriptor
    {
        public static string GetEnumDescription(Enum enumObj)
        {
            FieldInfo fieldInfo = enumObj.GetType().GetField(enumObj.ToString());
            var attribArray = fieldInfo.GetCustomAttributes(false);

            if (attribArray.Length == 0)
                return enumObj.ToString();
            var attrib = attribArray[0] as DescriptionAttribute;
            return attrib != null ? attrib.Description : "No description";
        }
    }
}
