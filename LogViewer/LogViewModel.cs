﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Documents;
using LogViewer.Annotations;
using Xceed.Wpf.DataGrid;

namespace LogViewer
{
    public class LogViewModel : ChangeNotifyer
    {
        private List<LogLine> _searchMatches;
        private Regex _filterRegex;
        private Regex _searchRegex;
        private ObservableCollection<LogLine> _displayedLines;
        private LogLine _selectedLine;
        private List<LogLine> _lines;
        private string _filterString;
        private string _searchString;

        public LogLine SelectedLine
        {
            get { return _selectedLine; }
            set
            {
                if (Equals(value, _selectedLine)) return;
                _selectedLine = value;
                OnPropertyChanged();
            }
        }

        public List<LogLine> Lines
        {
            get { return _lines; }
            set
            {
                if (Equals(value, _lines)) return;
                _lines = value;
                UpdateDisplayedLines();
                OnPropertyChanged();
            }
        }

        public string FilterString
        {
            get { return _filterString; }
            set
            {
                if (value == _filterString) return;
                if (RegexValidator.IsValidRegex(value))
                {
                    _filterString = value;
                    _filterRegex = new Regex(value);
                }
                else
                {
                    _filterString = "";
                    _filterRegex = null;
                }
                UpdateDisplayedLines();
                OnPropertyChanged();
            }
        }

        public string SearchString
        {
            get { return _searchString; }
            set
            {
                if (value == _searchString) return;
                if (value.Trim().Length > 0 && RegexValidator.IsValidRegex(value))
                {
                    _searchString = value;
                    _searchRegex = new Regex(value);
                }
                else
                {
                    _searchString = "";
                    _searchRegex = null;
                }
                UpdateDisplayedLines();
                OnPropertyChanged();
            }
        }

        public ObservableCollection<LogLine> DisplayedLines
        {
            get { return _displayedLines; }
            set { _displayedLines = value;
                OnPropertyChanged();
            }
        }

        public LogViewModel(List<LogLine> lines)
        {
            _lines = lines;
            _searchMatches = new List<LogLine>();
            _filterRegex = null;
            _searchRegex = null;
            DisplayedLines = new ObservableCollection<LogLine>();
        }

        private void UpdateDisplayedLines()
        {
            DisplayedLines = new ObservableCollection<LogLine>();

            if (_filterRegex != null)
            {
                foreach (LogLine bString in Lines)
                    if (_filterRegex.IsMatch(bString.Text))
                        DisplayedLines.Add(bString);
            }
            else
                foreach (LogLine bString in Lines)
                    DisplayedLines.Add(bString);

            foreach (LogLine searchMatch in _searchMatches)
                searchMatch.IsSearchMatch = false;
            _searchMatches = new List<LogLine>();
            if (_searchRegex != null)
            {
                foreach (LogLine bString in DisplayedLines)
                    if (_searchRegex.IsMatch(bString.Text))
                    {
                        _searchMatches.Add(bString);
                        bString.IsSearchMatch = true;
                    }
            }
        }

        public void NextSearchMatch()
        {
            if (_searchMatches.Count == 0) return;
            if (SelectedLine == null)
                SelectedLine = _searchMatches.First();
            else
            {
                if (_searchMatches.Contains(SelectedLine))
                {
                    var index = _searchMatches.IndexOf(SelectedLine)+1;
                    SelectedLine = index == _searchMatches.Count ? _searchMatches.First() : _searchMatches[index];
                }
                else
                    SelectedLine = _searchMatches.First();
            }
        }

        public void PreviousSearchMatch()
        {
            if (_searchMatches.Count == 0) return;
            if (SelectedLine == null)
                SelectedLine = _searchMatches.Last();
            else
            {
                if (_searchMatches.Contains(SelectedLine))
                {
                    var index = _searchMatches.IndexOf(SelectedLine) - 1;
                    SelectedLine = index == -1 ? _searchMatches.Last() : _searchMatches[index];
                }
                else
                    SelectedLine = _searchMatches.Last();
            }
        }
    }
}
