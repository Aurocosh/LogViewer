﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace LogViewer
{
    public class RuleSetSerializer
    {
        private readonly XmlSerializer _serializer;
        public string LastError;

        public RuleSetSerializer()
        {
            var ruleTypes = new[] {typeof(Rule)};
            _serializer = new XmlSerializer(typeof(RuleSet), ruleTypes);
            LastError = "";
        }

        public bool Serialize(RuleSet ruleSet, string fileName)
        {
            try
            {
                using (var fileStream = new FileStream(fileName, FileMode.Create))
                {
                    _serializer.Serialize(fileStream, ruleSet);
                }
                return true;
            }
            catch (Exception exception)
            {
                LastError = exception.ToString();
                return false;
            }
        }

        public RuleSet Deserialize(string fileName)
        {
            try
            {
                using (var fileStream = new FileStream(fileName, FileMode.Open))
                {
                    return (RuleSet)_serializer.Deserialize(fileStream);
                }
            }
            catch (Exception exception)
            {
                LastError = exception.ToString();
                return null;
            }
        }
    }
}
