﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LogViewer
{
    public class RuleContainerSerializer
    {
        private readonly XmlSerializer _serializer;
        public string LastError;

        public RuleContainerSerializer()
        {
            var ruleTypes = new[] { typeof(Rule), typeof(RuleSet) };
            _serializer = new XmlSerializer(typeof(RuleSetsContainer), ruleTypes);
            LastError = "";
        }

        public bool Serialize(RuleSetsContainer ruleSets, string fileName)
        {
            try
            {
                using (var fileStream = new FileStream(fileName, FileMode.Create))
                {
                    _serializer.Serialize(fileStream, ruleSets);
                }
                return true;
            }
            catch (Exception exception)
            {
                LastError = exception.ToString();
                return false;
            }
        }

        public RuleSetsContainer Deserialize(string fileName)
        {
            try
            {
                using (var fileStream = new FileStream(fileName, FileMode.Open))
                {
                    return (RuleSetsContainer)_serializer.Deserialize(fileStream);
                }
            }
            catch (Exception exception)
            {
                LastError = exception.ToString();
                return null;
            }
        }
    }
}
