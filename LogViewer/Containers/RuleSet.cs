﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Documents;
using System.Xml.Serialization;

namespace LogViewer
{
    [XmlRoot("RuleSet")]
    [XmlInclude(typeof(Rule))]
    public class RuleSet : ChangeNotifyer, ICloneable
    {
        private string _name;
        private string _description;
        private ObservableCollection<Rule> _rules;

        [XmlAttribute("Name")]
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged();}
        }

        [XmlAttribute("Description")]
        public string Description
        {
            get { return _description; }
            set { _description = value; OnPropertyChanged();}
        }

        [XmlArray("Rules")]
        [XmlArrayItem("rule")]
        public ObservableCollection<Rule> Rules
        {
            get { return _rules; }
            set { _rules = value; OnPropertyChanged();}
        }

        public RuleSet()
        {
            Name = "Noname";
            Description = "Description";
            _rules = new ObservableCollection<Rule>();
        }

        public RuleSet(string name)
        {
            _name = name;
            Description = "Description";
            _rules = new ObservableCollection<Rule>();
        }

        public RuleSet(string name, string description)
        {
            _name = name;
            _description = description;
            _rules = new ObservableCollection<Rule>();
        }

        public RuleSet(string name, string description, ObservableCollection<Rule> rules)
        {
            _name = name;
            _description = description;
            _rules = rules;
        }

        public void UpdateRuleSet()
        {
            foreach (Rule rule in Rules)
                rule.UpdateRule();
        }

        public object Clone()
        {
            var rules = new ObservableCollection<Rule>();
            foreach (Rule rule in _rules)
                rules.Add((Rule)rule.Clone());
            return new RuleSet(_name, Description, rules);
        }
    }
}
