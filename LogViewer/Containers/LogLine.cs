﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace LogViewer
{
    public class LogLine
    {
        public int Index { get; private set; }
        public string Text { get; set; }
        public bool IsSearchMatch { get; set; }
        public LogLine(int index, string value)
        {
            Index = index;
            Text = value;
        }
    }
}
