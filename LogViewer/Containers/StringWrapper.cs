﻿using System;
using System.Xml.Serialization;

namespace LogViewer
{
    [XmlType("StringWrapper")]
    public class StringWrapper : ChangeNotifyer, ICloneable
    {
        private string _value;

        [XmlAttribute("Value")]
        public string Value
        {
            get { return _value; }
            set { _value = value; OnPropertyChanged();}
        }

        public StringWrapper()
        {
            _value = "";
        }

        public StringWrapper(string value)
        {
            _value = value;
        }

        public object Clone()
        {
            return new StringWrapper(_value);
        }
    }
}
