﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Serialization;

namespace LogViewer
{
    [XmlRoot("RuleSetsContainer")]
    [XmlInclude(typeof(RuleSet))]
    public class RuleSetsContainer : ChangeNotifyer, ICloneable
    {
        private RuleSet _currentRuleSet;

        [XmlArray("RuleSets")]
        [XmlArrayItem("Rules")]
        public ObservableCollection<RuleSet> RuleSets { get; set; }

        [XmlIgnore]
        public RuleSet CurrentRuleSet
        {
            get { return _currentRuleSet; }
            set { _currentRuleSet = value; OnPropertyChanged();}
        }

        public RuleSetsContainer()
        {
        }

        public RuleSetsContainer(ObservableCollection<RuleSet> ruleSets)
        {
            RuleSets = ruleSets;
            CurrentRuleSet = ruleSets.FirstOrDefault();
        }

        public void UpdateRuleSets()
        {
            foreach (RuleSet ruleSet in RuleSets)
                ruleSet.UpdateRuleSet();
        }

        public object Clone()
        {
            var ruleSets = new ObservableCollection<RuleSet>();
            foreach (RuleSet ruleSet in RuleSets)
                ruleSets.Add((RuleSet)ruleSet.Clone());
            return new RuleSetsContainer(ruleSets);
        }
    }
}
