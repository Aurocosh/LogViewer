﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Xml.Serialization;
using LogViewer.Annotations;

namespace LogViewer
{
    [XmlType("Rule")]
    [XmlInclude(typeof(StringWrapper))]
    public class Rule : ChangeNotifyer, ICloneable
    {
        private string _name;
        private Color _highlightColor;
        private string _regexText;
        private ObservableCollection<StringWrapper> _keyWords;
        private RuleType _type;

        [XmlIgnore]
        public Regex Regex { get; private set; }

        [XmlAttribute("Name")]
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(); }
        }

        [XmlElement("RegexText")]
        public string RegexText
        {
            get { return _regexText; }
            set { _regexText = value; OnPropertyChanged(); }
        }

        [XmlIgnore]
        public Color HighlightColor
        {
            get { return _highlightColor; }
            set { _highlightColor = value; OnPropertyChanged(); }
        }

        [XmlAttribute("Color")]
        public string HighlightColorString
        {
            get
            {
                var colorConverter = new ColorConverter();
                return colorConverter.ConvertToString(HighlightColor);
            }
            set
            {
                object convertFromString = ColorConverter.ConvertFromString(value);
                if (convertFromString != null)
                    HighlightColor = (Color)convertFromString;
                else
                    HighlightColor = Colors.DarkRed;
            }
        }

        [XmlArray("KeyWords")]
        [XmlArrayItem("keyword")]
        public ObservableCollection<StringWrapper> KeyWords
        {
            get { return _keyWords; }
            set { _keyWords = value; OnPropertyChanged(); }
        }

        [XmlAttribute("Type")]
        public RuleType Type
        {
            get { return _type; }
            set { _type = value; OnPropertyChanged(); }
        }

        public Rule()
        {
            _keyWords = new ObservableCollection<StringWrapper>();
        }

        public Rule(string name, RuleType type)
        {
            _name = name;
            _type = type;
            _highlightColor = Colors.Blue;
            _keyWords = new ObservableCollection<StringWrapper>();
        }

        public Rule(string name, Color highlightColor, string regexText, ObservableCollection<StringWrapper> keyWords, RuleType type, Regex regex)
        {
            _name = name;
            _highlightColor = highlightColor;
            _regexText = regexText;
            _keyWords = keyWords;
            _type = type;
            Regex = regex;
        }

        public void UpdateRule()
        {
            var regexText = "";
            if (Type == RuleType.RegexRule)
                regexText = "(" + RegexText + ")";
            else if (KeyWords.Count != 0)
                regexText = "(" + string.Join("|", KeyWords.Select(v => v.Value)) + ")";

            Regex = RegexValidator.IsValidRegex(regexText) ? new Regex(regexText) : null;
        }

        public object Clone()
        {
            var keyWords = new ObservableCollection<StringWrapper>();
            foreach (StringWrapper keyWord in keyWords)
                keyWords.Add((StringWrapper)keyWord.Clone());
            return new Rule(_name, _highlightColor, _regexText, keyWords, _type, Regex);
        }
    }
}
